const os = require("os");
const colors = require("colors");
process.stdin.setEncoding("utf-8");

process.stdin.setEncoding("utf-8");

function getOSinfo() {
  var type = os.type(); //lepiej w formie var na poczatku, czy w srodku const?
  var release = os.release();
  var cpu = os.cpus()[0].model;
  var currentUser = os.userInfo();
  if (type === "Darwin") {
    type = "OSX";
  } else if (type === "Windows_NT") {
    type = "Windows";
  }
  console.log("System:".green, type);
  console.log("Release:".blue, release);
  console.log("CPU:".red, cpu);
  console.log("User:".yellow, currentUser.username);
  console.log("Homedir:".rainbow, currentUser.homedir);
  //   process.stdout.write(`CPU: ${cpu.model}\n`);
  //   process.stdout.write(`User: ${currentUser.username}\n`);
  //   process.stdout.write(`Homedir: ${currentUser.homedir}\n`);
}

exports.print = getOSinfo;

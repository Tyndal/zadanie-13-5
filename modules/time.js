const os = require("os");
const colors = require("colors");
process.stdin.setEncoding("utf-8");

function getUptime() {
  var uptime = os.uptime();
  var timeSecondsFloor = Math.floor(uptime);
  var timeHours = Math.floor(timeSecondsFloor / 60 / 60);
  var timeMinutes = Math.floor(timeSecondsFloor / 60 - timeHours * 60);
  var timeSeconds = Math.floor(
    timeSecondsFloor - (timeHours * 60 * 60 + timeMinutes * 60)
  );

  console.log(
    "Uptime: ".cyan +
      timeHours +
      " hour(s) " +
      timeMinutes +
      " minutes " +
      timeSeconds +
      " seconds."
  );
}

exports.uptime = getUptime;
